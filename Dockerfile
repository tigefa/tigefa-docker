FROM ubuntu:16.04
MAINTAINER Sugeng Tigefa <sugeng@tigefa.space>

ENV DEBIAN_FRONTEND noninteractive

RUN cp /etc/apt/sources.list /etc/apt/sources.list.default
COPY sources.list /etc/apt/sources.list

# Install apt packages
RUN apt update         && \
    apt upgrade -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold"  && \
    apt install -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold"     \
        coreutils             \
        util-linux            \
        bsdutils              \
        file                  \
        openssl               \
        ca-certificates       \
        tk-dev                \
        wget                  \
        patch                 \
        sudo                  \
        zlib1g-dev            \
        libssl-dev            \
        make                  \
        language-pack-id      \
        curl                  \
        git                   \
        zlib1g-dev            \
        realpath              \
        libreadline-dev       \
        libsqlite3-dev        \
        libncurses5-dev       \
        libncursesw5-dev      \
        llvm                  \
        xz-utils              \
        build-essential       \
        p7zip                 \
        p7zip-full            \
        bash-completion       && \
    apt-get clean

# Set locale
RUN locale-gen id_ID.UTF-8

# Print tool versions
RUN git --version
RUN curl --version
RUN wget --version

# Add user "pyenv" as non-root user
#RUN useradd -ms /bin/bash pyenv

# Set sudoer for "pyenv"
#RUN echo 'pyenv ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers

# Switch to user "pyenv" from now
#USER pyenv

# pyenv
#RUN sudo su -
#RUN git clone https://github.com/pyenv/pyenv.git $HOME/.pyenv
#RUN export PYENV_ROOT="$HOME/.pyenv"
#RUN export PATH="$PYENV_ROOT/bin:$PATH"
#RUN pyenv init -
#RUN echo 'export PYENV_ROOT="$HOME/.pyenv"'      >> $HOME/.bashrc
#RUN echo 'export PATH="$PYENV_ROOT/bin:$PATH"'   >> $HOME/.bashrc
#RUN sudo su -
#USER pyenv
#RUN sudo su - pyenv
#RUN echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n  eval "$(pyenv init -)"\nfi' >> $HOME/.bashrc
#RUN cat $HOME/.bashrc
#RUN source $HOME/.bashrc
#RUN sudo su -
#RUN pyenv --version

# Build Python
#USER pyenv
#RUN sudo su - pyenv
#RUN pyenv install 2.7-dev

#RUN pyenv global 2.7-dev
#RUN python --version
#RUN ls $PYENV_ROOT/versions
#RUN cd $PYENV_ROOT/versions && \
#    7z a python-2.7-dev.7z 2.7-dev

# Set WORKDIR to pyenv directory
#WORKDIR /home/pyenv

#ENTRYPOINT /bin/bash
